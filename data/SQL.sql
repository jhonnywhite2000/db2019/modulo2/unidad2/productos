﻿DROP DATABASE IF EXISTS productos;
CREATE DATABASE productos;
USE productos;

CREATE OR REPLACE TABLE producto(
  idProducto int AUTO_INCREMENT,
  nombre int,
  foto varchar(150),
  descripcion text,
  precio float,
  fechaAlta date,
  PRIMARY KEY(idProducto)
);

CREATE OR REPLACE TABLE pedidos(
  idPedido int AUTO_INCREMENT,
  producto int,
  tienda int,
  precioCompra float,
  cantidad int,
  fechaPedido date,
  horaPedido time,
  PRIMARY KEY(idPedido),
  UNIQUE KEY (producto,tienda,fechaPedido,horaPedido)
);

CREATE OR REPLACE TABLE tienda(
  idTienda int AUTO_INCREMENT,
  direccion varchar(150),
  poblacion varchar(50),
  telefono varchar(20),
  email varchar(50),
  PRIMARY KEY(idTienda)
);

CREATE OR REPLACE TABLE stock(
  id int AUTO_INCREMENT,
  producto int,
  tienda int,
  stock int,
  precio float,
  PRIMARY KEY(id),
  UNIQUE KEY (producto,tienda)
);

ALTER TABLE pedidos
  ADD CONSTRAINT fkpedidostienda
  FOREIGN KEY (tienda)
  REFERENCES tienda(idTienda),
  ADD CONSTRAINT fkpedidosproducto
  FOREIGN KEY (producto)
  REFERENCES producto(idProducto);

ALTER TABLE stock
  ADD CONSTRAINT fkstocktienda
  FOREIGN KEY (tienda)
  REFERENCES tienda(idTienda),
  ADD CONSTRAINT fkstockproducto
  FOREIGN KEY (producto)
  REFERENCES producto(idProducto);




