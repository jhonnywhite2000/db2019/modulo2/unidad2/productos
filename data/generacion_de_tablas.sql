﻿DROP DATABASE IF EXISTS productos;
CREATE DATABASE productos;

USE productos;

CREATE OR REPLACE TABLE producto(
  idproducto int AUTO_INCREMENT,
  nombre int,
  foto varchar(150),
  descripcion text,
  precio float,
  fechaAlta date,
  PRIMARY KEY(idproducto)
);

CREATE OR REPLACE TABLE pedidos(
  idPedido int AUTO_INCREMENT,
  producto int,
  tienda int,
  precioCompra float,
  cantidad int,
  fechaPedido date,
  horaPedido time,
  PRIMARY KEY(idPedido)
);

CREATE OR REPLACE TABLE tienda(
  idTienda int AUTO_INCREMENT,
  producto int,
  direccion varchar(100),
  poblacion varchar(100),
  telefono varchar(50),
  email varchar(50),
  PRIMARY KEY(idTienda)
);

CREATE OR REPLACE TABLE stock(
  id int AUTO_INCREMENT,
  producto int,
  tienda int,
  stock int,
  precioVenta float,
  PRIMARY KEY(id)
);

ALTER TABLE pedidos
  ADD CONSTRAINT FKpedidosTienda
  FOREIGN KEY(tienda) REFERENCES tienda(idTienda) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT FKpedidosproducto
  FOREIGN KEY(producto) REFERENCES producto(idproducto) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT producto 
  UNIQUE KEY(producto, tienda, fechaPedido, horaPedido);
 

ALTER TABLE stock
  ADD CONSTRAINT FKstockTienda
  FOREIGN KEY(tienda) REFERENCES tienda(idTienda) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT FKstockproducto
  FOREIGN KEY(producto) REFERENCES producto(idproducto) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT producto
  UNIQUE KEY (producto,tienda);